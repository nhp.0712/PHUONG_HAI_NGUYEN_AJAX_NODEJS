var express = require('express');
var router = express.Router();

var fs      = require('fs');
var xml2js  = require('xml2js');
var parser  = new xml2js.Parser();

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', { title: 'PHUONG NGUYEN, Assignment #11' });
});


router.get('/books.xml', function(req, res, next) {

    var xmlfile = __dirname + "/../public/xml/books.xml";
    
    fs.readFile(xmlfile, "utf-8", function (error, text) {

        if (error) {
            throw error;
        }else {
            res.set("Content-type", "text/xml");
            res.send(text);
        }
    });
});

router.get('/books.json', function(req, res, next) {

    var jsonFile = __dirname + "/../public/json/books.json";
    fs.readFile(jsonFile, "utf-8", function (error, text) {
        if (error) {
            throw error;
        }else {
            var books = JSON.parse(text);
            console.log(books);
            res.json(books);
        }
    });

});
router.get('/donuts.xml', function(req, res, next) {

    var xmlfile = __dirname + "/../public/xml/donuts.xml";
    
    fs.readFile(xmlfile, "utf-8", function (error, text) {

        if (error) {
            throw error;
        }else {
            res.set("Content-type", "text/xml");
            res.send(text);
        }
    });
});

router.get('/donuts.json', function(req, res, next) {

    var jsonFile = __dirname + "/../public/json/donuts.json";
    fs.readFile(jsonFile, "utf-8", function (error, text) {
        if (error) {
            throw error;
        }else {
            var donuts = JSON.parse(text);
            console.log(donuts);
            res.json(donuts);
        }
    });

});

module.exports = router;
